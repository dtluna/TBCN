#!/usr/bin/env python
# -- coding: utf-8 --
from serial import Serial
from os import geteuid, environ, execlpe
from sys import executable, argv


def run_as_root():
    euid = geteuid()
    if euid != 0:
        print("Script not started as root. Running sudo..")
        args = ['sudo', executable] + argv + [environ]
        execlpe('sudo', *args)

if __name__ == '__main__':
    #run_as_root()
    outport = '/dev/tnt0'
    inport = '/dev/tnt1'

    out_serial = Serial(outport, 38400)
    in_serial = Serial(inport, 38400)

    out_serial.write(str(0x5e))
    byte = in_serial.read()

    print(byte)
