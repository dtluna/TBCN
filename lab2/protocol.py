from serial import Serial

outport = '/dev/pts/2'
inport = '/dev/pts/3'
out_serial = Serial(outport, 38400)
in_serial = Serial(inport, 38400)

FLAG = chr(0x7e)
ESC = chr(0x7d)
FLAG_DETER = chr(0x5e)
ESC_DETER = chr(0x5d)

PACKAGE_LEN = 16


def to_hex(s):
    return " ".join([hex(ord(c))[2:].zfill(2) for c in s])


def stuff_esc_bytes(data):
    return data.replace('%c' % ESC, '%c%c' % (ESC, ESC_DETER))


def stuff_flag_bytes(data):
    return data.replace('%c' % FLAG, '%c%c' % (ESC, FLAG_DETER))


def stuff_bytes(data):
    data = stuff_esc_bytes(data)
    data = stuff_flag_bytes(data)
    return data


def decode_flag_bytes(data):
    return data.replace('%c%c' % (ESC, FLAG_DETER), '%c' % FLAG)


def decode_esc_bytes(data):
    return data.replace('%c%c' % (ESC, ESC_DETER), '%c' % ESC)


def decode_package(data):
    data = decode_flag_bytes(data)
    data = decode_esc_bytes(data)
    return data


def send_package(data):
    i = 0
    package_data = data[i:i+PACKAGE_LEN]
    while(package_data):
        print('Sending message:\n' + package_data)
        print('Your message in hex form:\n' + to_hex(package_data))
        package_data = stuff_bytes(package_data)
        print('Your message after stuffing:\n' + to_hex(package_data))
        package_data = '%c%s%c' % (FLAG, package_data, FLAG)
        print('Full package:\n' + to_hex(package_data))
        out_serial.write(package_data)
        i += PACKAGE_LEN
        package_data = data[i:i+PACKAGE_LEN]


def is_flag(byte):
    return byte == FLAG


def receive_package():
    data = ''
    byte = in_serial.read()
    if is_flag(byte):
        byte = in_serial.read()
        while is_flag(byte) == False:
            data += byte
            byte = in_serial.read()
    package_data = '%c%s%c' % (FLAG, data, FLAG)
    print('Full received package:\n%s' % to_hex(package_data))
    print('Package data:\n' + to_hex(data))
    return decode_package(data)
