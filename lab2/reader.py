#!/usr/bin/env python2
from protocol import receive_package, to_hex

if __name__ == '__main__':
    print('Press Ctrl-\\ to close the program.')
    while True:
        data = receive_package()
        print('Your message in hex:\n' + to_hex(data))
        print('Your message in text form:\n' + data)
